<?php

// Print Deals Template.
add_action( 'auto_listings_single_content', function() {
	$deals = auto_listings_get_listing_deals();
	$contact_for_deals = "<div class='contact-for-deals'>Call: <a href='tel:03302291800'>0330 229 1800</a> for deals or use the enquiry form <img src='https://affordablelease.co.uk/wp-content/uploads/2020/04/arrowright-1.png' src='https://affordablelease.co.uk/wp-content/uploads/2020/04/arrowright-1.png'></div>";
	if ( empty( $deals ) ) {
		echo($contact_for_deals);
		return;
	}
	// $deals = auto_listings_get_listing_deals();
	// if ( empty( $deals ) ) {
	// 	return;
	// }
	?>
	<div class="al-deals-wrapper">
		<h3><?php _e( 'Leasing Deals', 'al-jack' ); ?></h3>

		<div class="al-deals">
			<?php foreach( $deals as $index => $deal ) : ?>
				<div class="al-deal">
					<div class="al-deal__index"><?php echo $index + 1; ?></div>

					<?php if ( ! empty( $deal['lease_term'] ) ) : ?>
						<div class="al-deal__attr al-deal__lease-term">
							<?php _e( 'Lease Term', 'al-jack' ); ?>
							<strong><?php echo esc_html( $deal['lease_term'] ); ?> years</strong>
						</div>
					<?php endif; ?>

					<?php if ( ! empty( $deal['fuel_type'] ) ) : ?>
						<div class="al-deal__attr al-deal__fuel-type">
							<?php _e( 'Fuel Type', 'al-jack' ); ?>
							<strong><?php echo esc_html( $deal['fuel_type'] ); ?></strong>
						</div>
					<?php endif; ?>

					<?php if ( ! empty( $deal['mileage'] ) ) : ?>
						<div class="al-deal__attr al-deal__mileage">
							<?php _e( 'Max Annual Mileage', 'al-jack' ); ?>
							<strong><?php echo esc_html( $deal['mileage'] ); ?></strong>
						</div>
					<?php endif; ?>

					<?php if ( ! empty( $deal['deal_type'] ) ) : ?>
						<div class="al-deal__attr al-deal__deal-type">
							<?php _e( 'Deal Type', 'al-jack' ); ?>
							<strong><?php echo esc_html( $deal['deal_type'] ); ?></strong>
						</div>
					<?php endif; ?>

					<?php if ( ! empty( $deal['deposit'] ) ) : ?>
						<div class="al-deal__attr al-deal__deposit">
							<?php _e( 'Deposit', 'al-jack' ); ?>
							<strong><?php echo esc_html( $deal['deposit'] ); ?></strong>
						</div>
					<?php endif; ?>

					<?php if ( ! empty( $deal['price'] ) ) : ?>
						<div class="al-deal__attr al-deal__deal-price">
							<?php //printf( __( '<strong>%s</strong> pcm', 'al-jack' ), auto_listings_format_price( $deal['price'] ) ); ?>
							<?php //_e( 'Price', 'al-jack' ); ?>
							<strong class="price-amount">£<?php echo esc_html( $deal['price'] ); ?></strong>
						</div>
					<?php endif; ?>
				</div>
			<?php endforeach; ?>
		</div>
		<div class="al-deals-footer">
			<p>Additional mileage and lease terms are available, please specify your requirements in the enquiry form<img src="https://affordablelease.co.uk/wp-content/uploads/2020/04/arrowright-1.png" src="https://affordablelease.co.uk/wp-content/uploads/2020/04/arrowright-1.png"></p>
		</div>
	</div>
	<?php
}, 25 );

// Replace normal price with the lowest price deal.
add_filter( 'auto_listings_price_html', function( $output ) {
	if ( auto_listings_hide_item( 'price' ) ) {
		return;
	}
	$min_deal = auto_listings_meta( 'min_deal' );
	if ( empty( $min_deal ) ) {
		return $output;
	}
	// Get the lowest price deal.
	return sprintf( __( 'from %s', 'al-jack' ), auto_listings_format_price( $min_deal ) );
} );

// Change the heading of Detail Tab.
add_filter( 'auto_listings_details_heading', function( $title ) {
	return __( 'Vehicle Details', 'al-jack' );
} );

// Change the heading of Specifications Tab.
add_filter( 'auto_listings_specifications_heading', function( $title ) {
	return __( 'Available Specifications', 'al-jack' );
} );

// Remove fields from Detail Metabox.
add_filter( 'auto_listings_metabox_details', function( $fields ) {
	unset( $fields[0] );
	unset( $fields[2] );
	unset( $fields[3] );
	unset( $fields[4] );
	return array_values( $fields );
} );

// Remove Year from Spec Metabox.
add_filter( 'auto_listings_metabox_specs', function( $fields ) {
	unset( $fields[0] );
	return array_values( $fields );
} );

// Remove Address Metabox.
add_action( 'add_meta_boxes', function() {
	remove_meta_box( '_al_listing_address', 'auto-listing', 'side' );
}, 99);

// Rename Vehicle in Specificatons to Trims.
add_filter( 'auto_listings_metabox_specs', function( $fields ) {
	$fields[2] = [
		'name'              => __( 'Trims', 'al-jack' ),
		'id'                => '_al_listing_model_vehicle',
		'type'              => 'text',
		'sanitize_callback' => 'wp_kses_post',
		'columns'           => 3,
	];
	return $fields;
} );

add_action( 'rwmb__al_listing_deals_after_save_post', function( $object_id ) {
	$deals = auto_listings_meta( 'deal', $object_id );
	if ( empty( $deals ) ) {
		return;
	}
	usort( $deals, function( $deal_1, $deal_2 ) {
		return absint( $deal_1['price'] ) - absint( $deal_2['price'] );
	} );

	update_post_meta( $object_id, '_al_listing_min_deal', floatval( $deals[0]['price'] ) );
	update_post_meta( $object_id, '_al_listing_price', floatval( $deals[0]['price'] ) );
} );
