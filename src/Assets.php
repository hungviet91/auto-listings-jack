<?php
namespace AutoListingsJack;

class Assets {
	public function __construct() {
		add_action( 'wp_enqueue_scripts', [ $this, 'enqueue' ] );
	}

	public function enqueue() {
		if ( ! is_auto_listings() && ! is_front_page() ) {
			return;
		}
		if ( is_singular( 'auto-listing' ) ) {
			wp_enqueue_style( 'al-jack-style', AL_JACK_URL . 'assets/css/style.css', array(), '' );
		}
		wp_enqueue_script( 'al-jack-script', AL_JACK_URL . 'assets/js/script.js', array( 'jquery' ), '1.0.0', true );
		wp_localize_script( 'al-jack-script', 'AL_JACK', array(
			'ajaxURL'         => admin_url( 'admin-ajax.php' ),
		) );
	}
}
