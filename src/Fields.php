<?php

namespace AutoListingsJack;

class Fields {
	public function __construct() {
		add_action( 'rwmb_meta_boxes', [ $this, 'register_for_listings' ] );
		add_filter( 'rwmb_meta_boxes', [ $this, 'register_settings_fields' ] );
		add_action( 'auto_listings_contact_fields', [ $this, 'register_for_enquiry' ] );
	}

	public function register_for_listings( $meta_boxes ) {
		$meta_boxes[] = [
			'id'         => '_al_listing_deals',
			'title'      => __( 'Deals', 'auto-listings' ),
			'post_types' => 'auto-listing',
			'priority'   => 'low',
			'fields'     => [
				[
					'type'        => 'group',
					'id'          => '_al_listing_deal',
					'clone'       => true,
					'collapsible' => true,
					'group_title' => __( 'Deal {#}', 'auto-listings' ),
					'add_button'  => __( '+ Add Deal', 'auto-listings' ),
					'fields'      => [
						[
							'name' => __( 'Deal Price', 'auto-listings' ),
							'id'   => 'price',
							'type' => 'text',
						],
						[
							'name' => __( 'Lease Term', 'auto-listings' ),
							'id'   => 'lease_term',
							'type' => 'number',
						],
						[
							'name' => __( 'Annual Mileage', 'auto-listings' ),
							'id'   => 'mileage',
							'type' => 'number',
						],
						[
							'name' => __( 'Fuel Type', 'auto-listings' ),
							'id'   => 'fuel_type',
							'type' => 'text',
						],
						[
							'name' => __( 'Deal Type', 'auto-listings' ),
							'id'   => 'deal_type',
							'type' => 'text',
						],
						[
							'name' => __( 'Deposit', 'auto-listings' ),
							'id'   => 'deposit',
							'type' => 'text',
						],
					],
				],
			],
		];

		return $meta_boxes;
	}

	public function register_for_enquiry( $fields ) {
		$fields[] =	[
			'name' => is_admin() ? __( 'GDPR Agreement', 'auto-listings' ) : '',
			'before' => '<span>' . __( 'I consent to Affordable Lease storing my submitted information so they can respond to my inquiry', 'al-jack' ) . '</span>',
			'id'   => '_al_enquiry_gdpr',
			'required' => true,
			'type' => 'checkbox',
		];

		$enquiry_id = rwmb_request()->filter_get( 'post', FILTER_SANITIZE_NUMBER_INT );
		$listing_id = get_post_meta( $enquiry_id, '_al_enquiry_listing_id', true );

		$deals = auto_listings_get_listing_deals( $listing_id );
		$link_before_field = sprintf('
			<div class="rwmb-field">
				<div class="rwmb-label">
					<label>Listing</label>
				</div>
				<div class="rwmb-input">
					<a href="%s">%s</a>
				</div>
			</div>
		', get_permalink( $listing_id ), get_the_title( $listing_id ) );

		if ( ! empty( $deals ) ) {
			$options = array_map( function( $deal ) {
				return sprintf( __( 'Deal %s', 'al-jack' ), $deal + 1 );
			}, array_keys( $deals ) );
			$options = array_merge( [ '' => __( 'Choose Deal', 'al-jack' ) ], $options );

			$fields[] =	[
				'name'    => is_admin() ? __( 'Interesting Deal', 'auto-listings' ) : '',
				'id'      => '_al_enquiry_interesting_deal',
				'type'    => 'select',
				'after'   => $link_before_field,
				'options' => $options,
			];
		}

		return $fields;
	}

	public function register_settings_fields( $meta_boxes ) {
		$meta_boxes[] = [
			'id'             => 'al_budget',
			'title'          => __( 'Budgets', 'auto-listings' ),
			'settings_pages' => 'auto-listings',
			'tab'            => 'general',
			'fields'         => [
				[
					'id'          => 'budget_range',
					'name'        => __( 'Budgets Range', 'auto-listings' ),
					'type'        => 'text',
					'desc' => __( 'Enter your budget range here, from lowest to highest, seperated by the comma. If this is left empty, our default budget range will be used.', 'auto-listings' ),
				],
			],
		];

		return $meta_boxes;
	}
}
