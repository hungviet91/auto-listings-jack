<?php
namespace AutoListingsJack;

class SearchQuery {

	public function __construct() {
		add_filter( 'auto_listings_search_query', [ $this, 'additional_search_query' ], 11 );
	}

	public function additional_search_query( $query ) {
		$budget_query[] = $this->budget_query();
		$make_model[]   = $this->make_model_query();

		return array_merge( $query, $budget_query, $make_model );
	}

	public function budget_query() {
		if ( isset( $_GET['budget'] ) && ! empty( $_GET['budget'] ) ) {
			$budget = absint( sanitize_text_field( wp_unslash( $_GET['budget'] ) ) );
			return [
				'key'     => '_al_listing_min_deal',
				'value'   => $budget,
				'compare' => '<=',
				'type'    => 'NUMERIC',
			];
		}
		return [];
	}

	public function make_model_query() {
		if ( ! isset( $_GET['make_model'] ) || empty( $_GET['make_model'] ) ) {
			return [];
		}
		$make_model = sanitize_text_field( $_GET['make_model'] );

		// User enter both make and model.
		$make_model_array = explode( ' ', $make_model );
		if ( count( $make_model_array ) > 2 ) {
			$custom_fields = [
				'_al_listing_make_display' => $make_model_array[0],
				'_al_listing_model_name' => $make_model_array[1],
			];
		} else {
			$custom_fields = [
				'_al_listing_make_display' => $make_model,
				'_al_listing_model_name' => $make_model,
			];
		}

		$meta_query = [ 'relation' => 'OR' ];
		foreach ( $custom_fields as $key => $value ) {
			array_push(
				$meta_query,
				[
					'key'     => $key,
					'value'   => $value,
					'compare' => 'LIKE',
				]
			);
		}
		return $meta_query;
	}
}
