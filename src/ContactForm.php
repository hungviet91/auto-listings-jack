<?php
namespace AutoListingsJack;

class ContactForm {
	public function __construct() {
		add_action( 'rwmb_frontend_before_submit_button', [ $this, 'listing_fields' ] );
		add_action( 'rwmb_frontend_after_save_post', [ $this, 'add_listing_data' ] );
	}

	/**
	 * Add hidden input before submit button.
	 *
	 * @param array $config frontend submission configuration.
	 */
	public function listing_fields( $config ) {
		if ( 'auto_listings_contact_form' !== $config['id'] ) {
			return;
		}

		$deals = auto_listings_get_listing_deals();
		if ( empty( $deals ) ) {
			return;
		}
		?>
		<select id="_al_enquiry_interesting_deal" class="rwmb-select" name="_al_enquiry_interesting_deal">
			<option value=""><?php _e( 'Choose Deal', 'al-jack' ); ?></option>
			<?php foreach( $deals as $index => $deal ) :
				$deal = 'Deal ' . ( $index + 1 );
				?>
				<option value="<?= esc_attr( $index ); ?>"><?php echo esc_html( $deal ); ?></option>
			<?php endforeach; ?>
		</select>
		<?php
	}

	/**
	 * Add listing data after saving post.
	 *
	 * @param object $enquiry post object.
	 */
	public function add_listing_data( $enquiry ) {
		if ( 'auto_listings_contact_form' !== $enquiry->config['id'] ) {
			return;
		}
		$deal = filter_input( INPUT_POST, '_al_enquiry_interesting_deal', FILTER_SANITIZE_NUMBER_INT );
		if ( ! $deal ) {
			return;
		}
		update_post_meta( $enquiry->post_id, '_al_enquiry_interesting_deal', $deal );
	}
}