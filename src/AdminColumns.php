<?php

namespace AutoListingsJack;


class AdminColumns {

	/**
	 * Add hooks when module is loaded.
	 */
	public function __construct() {
		add_filter( 'manage_auto-listing_posts_columns', [ $this, 'listing_columns' ] );
		add_action( 'manage_auto-listing_posts_custom_column', [ $this, 'listing_show' ], 10, 2 );

		add_filter( 'manage_listing-enquiry_posts_columns', [ $this, 'enquiry_columns' ] );
		add_action( 'manage_listing-enquiry_posts_custom_column', [ $this, 'enquiry_show' ], 10, 2 );
	}

	/**
	 * Add columns.
	 *
	 * @param array $columns array of columns.
	 */
	public function listing_columns( $columns ) {
		unset( $columns['address'] );
		unset( $columns['price'] );
		$columns['deals'] = __( 'Deals', 'auto-listings' );
		$columns['min_deal'] = __( 'Min Deal', 'auto-listings' );
		return $columns;
	}

	/**
	 * Display output of custom columns.
	 *
	 * @param string $column_name Column name.
	 * @param int    $post_id     ID of the currently-listed post.
	 */
	public function listing_show( $column_name, $post_id ) {
		if ( 'deals' === $column_name ) {
			$deals = auto_listings_meta( 'deal', $post_id );
			$deals = $deals ? count( $deals ) : 0;
			echo $deals . __( ' Deals', 'al-jack' );
		}

		if ( 'min_deal' === $column_name ) {
			$min_deal = auto_listings_meta( 'min_deal', $post_id );
			if ( empty( $min_deal ) ) {
				$price = auto_listings_meta( 'price', $post_id );
				echo auto_listings_price( $price ); // wpcs xss: ok.
				return;
			}
			// Get the lowest price deal.
			echo sprintf( __( 'From %s', 'al-jack' ), auto_listings_format_price( $min_deal ) );
		}
	}

	public function enquiry_columns( $columns ) {
		$columns['deal'] = __( 'Deal', 'auto-listings' );
		return $columns;
	}

 	public function enquiry_show( $column_name, $post_id ) {
		if ( 'deal' === $column_name ) {
			$deal = get_post_meta( $post_id, '_al_enquiry_interesting_deal', true );
			echo $deal ? 'Deal ' . ( $deal + 1 ) : '';
		}
	}
}
