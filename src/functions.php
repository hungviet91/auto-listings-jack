<?php

/**
 * Get price min max search values
 */
function auto_listings_search_budgets() {
	$budget_range = auto_listings_option( 'budget_range' );
	$options = [
		'250' => auto_listings_raw_price( '250' ),
		'300' => auto_listings_raw_price( '300' ),
		'350' => auto_listings_raw_price( '350' ),
		'400' => auto_listings_raw_price( '400' ),
		'500' => auto_listings_raw_price( '500' ),
		'600' => auto_listings_raw_price( '600' ),
		'700' => auto_listings_raw_price( '700' ),
	];
	if ( empty( $budget_range ) ) {
		return $options;
	}
	$options = auto_listings_get_options_from_budget_range( $budget_range );
	return $options;
}

/**
 * Get options from budget range
 *
 * @param string $budget_range budget Range setting options.
 */
function auto_listings_get_options_from_budget_range( $budget_range ) {
	$budgets = explode( ',', $budget_range );
	$budgets = array_values( $budgets );
	sort( $budgets );

	$raw_budgets = array_map( function( $budget ) {
		return auto_listings_raw_price( $budget );
	}, $budgets );

	$options = array_combine( $budgets, $raw_budgets );
	return $options;
}

function auto_listings_get_listing_deals( $id = null ) {
	if ( ! $id ) {
		$id = get_the_ID();
	}
	$deals = auto_listings_meta( 'deal', $id );
	if ( empty( $deals ) ) {
		return;
	}
	usort( $deals, function( $deal_1, $deal_2 ) {
		return absint( $deal_1['price'] ) - absint( $deal_2['price'] );
	} );
	return $deals;
}