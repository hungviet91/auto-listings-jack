( function( $ ) {
	'use strict';

	var handleAjax = {
		$form        : $( '.auto-listings-search' ),
		$selectFields: $( '.auto-listings-search select' ),
		$submitButton: $( '.auto-listings-search .al-button' ),
		init: function() {
			handleAjax.filterByMake();
		},

		handleCarNumber: function() {
			handleAjax.$form.on( 'change', handleAjax.$selectFields, function() {
				$.post( {
					url: AL_MATTHEW.ajaxURL,
					data: {
						_wpnonce : AL_MATTHEW.handleCarNumber,
						action   : 'handle_car_number',
						data     : handleAjax.$form.serialize(),
					},
					success: function( response ) {
						if ( ! response.success ) {
							handleAjax.$submitButton
								.addClass( 'btn--no-car' )
								.html( response.data );
						} else {
							handleAjax.$submitButton.html( 'Show me ' + response.data + ' Cars' );
						}
					},
				} );
			} );
		},
		filterByMake: function() {
			$( '.make select' ).each( function() {
				const $this = $( this );
				const $modelSelect = handleAjax.$form.find( '.model select' );
				$modelSelect[0].sumo.disable();

				$this.on( 'change', function() {
					const selected = $( this ).val();
					const data = {
						action: 'model_filter',
						selected: selected
					};
					const $request = $.post( AL_JACK.ajaxURL, data );
					$request.done( function( r ) {
						handleAjax.handleMakeFilterRequest( r, $modelSelect );
					} );
				} );
			} );
		},
		handleMakeFilterRequest: function( r, modelSelect ) {
			const options = r.data;
			const isOption = options.search( /option/g );
			if ( isOption < 0 ) {
				modelSelect[0].sumo.disable();
				return;
			}
			modelSelect.html( options );
			modelSelect[0].sumo.reload();
			modelSelect[0].sumo.enable();
		}
	}

	$( '#_al_enquiry_interesting_deal' ).detach().insertBefore( '#auto-listings-contact > form > .rwmb-meta-box > span');

	$( window ).on( 'load', function() {
		handleAjax.init();
	} );
}( jQuery ) );
