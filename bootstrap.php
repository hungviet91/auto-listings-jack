<?php
namespace AutoListingsJack;

include 'vendor/autoload.php';

new Assets();
new Fields();
new Ajax();
new AdminColumns();
new SearchForm();
new SearchQuery();
new ContactForm();

require 'src/functions.php';
require 'src/template-hooks.php';
