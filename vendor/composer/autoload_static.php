<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit1478cfc680480d01ae3262d340a58d05
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'AutoListingsJack\\' => 17,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'AutoListingsJack\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit1478cfc680480d01ae3262d340a58d05::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit1478cfc680480d01ae3262d340a58d05::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
