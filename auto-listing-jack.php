<?php
/**
 * Plugin Name: Auto Listings Jack
 * Description:
 * Author: WP Auto Listings
 * Author URI: https://wpautolistings.com
 * Plugin URI: https://wpautolistings.com
 * Version: 1.0.0
 * Text Domain: auto-listings-jack
 * Domain Path: languages
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) exit;

define( 'AL_JACK_DIR', plugin_dir_path( __FILE__ ) );
define( 'AL_JACK_URL', plugin_dir_url( __FILE__ ) );

/**
 * Run the extension after main plugin is loaded.
 */
add_action( 'auto_listings_init', function() {
	include __DIR__ . '/bootstrap.php';
} );
